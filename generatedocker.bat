rem build polymer
rem polymer build

rem kill old docker
docker kill postgresql

rem remove old docker
docker rm postgresql

rem create a new build
docker build -t torru/postgresql .

rem start new container
rem --net techui
docker run -p 5432:5432 --name postgresql -d torru/postgresql
