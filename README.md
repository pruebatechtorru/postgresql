Creates a docker with a postgre sql
Creates an user, bbdd and table 
This bbdd serve as an authentication system


create table users (email VARCHAR(50),password VARCHAR(30), name VARCHAR(50), surname VARCHAR(50), role VARCHAR(50));
    
insert into users values ('miguelangel.lopez.martinez@bbva.com','1234','Miguel Angel','Lopez Martinez','admin');

insert into users values ('torrubiano69@gmail.com','1234','Miguel Angel','Lopez Martinez','user');


CREATE DATABASE docker
    WITH 
    OWNER = docker
    ENCODING = 'UTF8'
    LC_COLLATE = 'C.UTF-8'
    LC_CTYPE = 'C.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


CREATE TABLE public.users
(
    email character varying(50) COLLATE pg_catalog."default",
    password character varying(30) COLLATE pg_catalog."default",
    name character varying(50) COLLATE pg_catalog."default",
    surname character varying(50) COLLATE pg_catalog."default",
    role character varying(50) COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to torru;